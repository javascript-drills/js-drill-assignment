// Implement a function that retrieves and prints the hobbies of individuals with a specific age, say 30 years old.

function hobbiesByAge( data, age ){
    let hobbies = [];

    for(let index=0; index<data.length; index++){
        if(data[index].age == age){
            hobbies.push(data[index].hobbies);
        }
    }
    return hobbies;
}

module.exports = { hobbiesByAge };