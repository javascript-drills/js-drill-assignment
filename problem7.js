// Write a function that accesses and prints the names and email addresses of individuals aged 25.
  
function getNameAndEmails( data, age ){
    let userInfo = [];

    for( let index=0; index<data.length; index++ ){
        if(data[index].age == age){
            userInfo.push( {
                name: data[index].name,
                email: data[index].email,
            });
        }
    }
    
    return userInfo;
}
module.exports = { getNameAndEmails };
