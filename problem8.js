// Implement a loop to access and log the city and country of each individual in the dataset.

function getCityAndCountry( data ){
    let userInfo = [];

    for( let index=0; index<data.length; index++){
        userInfo.push({
            city: data[index].city,
            country: data[index].country,
        });
    }

    return userInfo;

}
module.exports = { getCityAndCountry };