// Write a function that accesses and logs the name and city of the individual at the index position 3 in the dataset.
  
function getNameAndCity( data, indexNo ){
    let userInfo = [];

    for( let index=0; index<data.length; index++ ){
        if(data[index].id == indexNo){
            userInfo = {
                name : data[index].name,
                city : data[index].city,
            };
        }
    }

    return userInfo;
}

module.exports = { getNameAndCity };