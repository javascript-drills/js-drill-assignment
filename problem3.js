// Create a function that extracts and displays the names of individuals who are students (`isStudent: true`) and live in Australia.

function getNameByCountry( data, country ){
    let stName = [];
    for( let index=0; index<data.length; index++ ){
        if( data[index].isStudent == true && data[index].country == country ){
            stName.push(data[index].name);
        }
    }
    return stName;
}

module.exports = { getNameByCountry };