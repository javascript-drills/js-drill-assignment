// Create a function to retrieve and display the first hobby of each individual in the dataset.

function getFirstHobby( data ){
    let firstHobby = [];

    for( let index=0; index<data.length; index++ ){
        firstHobby.push(data[index].hobbies[0]);
    }
    return firstHobby;
}
module.exports = { getFirstHobby };