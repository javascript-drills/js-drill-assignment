// Implement a loop to access and print the ages of all individuals in the dataset.
  
function getAges ( data ){
    let ages = [];

    for( let index=0; index<data.length; index++ ){
      ages.push(data[index].age);
    }

    return ages;
}
module.exports = { getAges };